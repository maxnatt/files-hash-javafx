/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.tables;

import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class AlgTableTest {
    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION = "jdbc:h2:mem:testfilesfx"; //;DB_CLOSE_DELAY=-1
    private static final String DB_USER = "sa";
    private static final String DB_PASSWORD = "";
    private Connection connection = null;

    @BeforeEach
    void setUp() {
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            String creationSql = "create table ALGORITHM" +
                    "(" +
                    "  ALG_ID   SMALLINT auto_increment," +
                    "  ALG_NAME VARCHAR  not null," +
                    "  constraint ALGORITHM_PK" +
                    "    primary key (ALG_ID)" +
                    ");";
            PreparedStatement preparedStatement = connection.prepareStatement(creationSql);
            preparedStatement.execute();
            preparedStatement.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    void tearDown() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    void getId() {
        int expectedId = 2;
        insert("DJB");
        insert("FNV");
        assertEquals(expectedId, AlgTable.getId("FNV", connection));
    }

    @Test
    void getName() {
        String expectedName = "DJB";
        insert(expectedName);
        assertEquals(expectedName, AlgTable.getName(1, connection));
    }

    private void insert(String algName) {
        String insertAlg = String.format("INSERT INTO PUBLIC.ALGORITHM (ALG_NAME) VALUES ('%s');", algName);

        try (PreparedStatement preparedStatement = connection.prepareStatement(insertAlg)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}