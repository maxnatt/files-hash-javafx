/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Represents connection to the embedded Hash database.
 */
public class DbConnection {
    private static final Logger logger = LoggerFactory.getLogger(DbConnection.class);

    /**
     * Contains JDBC driver's name used for DB connection.
     */
    private static final String JDBC_DRIVER = AppProperties.getProperty("jdbcDriver");

    /**
     * Contains full path to the DB file in user's home folder.
     */
    private static final String DB_URL = AppProperties.getProperty("dbBaseUrl") + System.getProperty("user.home") + System.getProperty("file.separator") + AppProperties.getProperty("dbBasePath");

    /**
     * Contains user's name for the DB connection.
     */
    private static final String USER = AppProperties.getProperty("dbUser");

    /**
     * Contains user's password for the DB connection.
     */
    private static final String PASS = AppProperties.getProperty("dbPass");

    /**
     * Represents connection to the DB.
     */
    private static Connection connection = null;

    /**
     * Connection getter which tries to use existing connection if possible.
     *
     * @return connection to the DB.
     */
    public static Connection getConnection() {
        if (connection != null)
            return connection;
        return createConnection();
    }

    /**
     * Creates and returns new connection to the DB.
     *
     * @return connection to the DB.
     */
    private static Connection createConnection() {
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    /**
     * Really closes the connection.
     */
    public static void destroyConnection() {
        try {
            if (connection != null) {
                logger.info("Closing DB connection now");
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
