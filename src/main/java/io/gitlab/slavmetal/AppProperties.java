/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Represents class for working with app's default properties.
 */
public class AppProperties {
    /**
     * File to search for in the resources.
     */
    private static final String PROP_NAME = "app.properties";

    /**
     * Object used to get properties from the file specified in the {@link AppProperties#PROP_NAME}.
     */
    private static final Properties PROP = new Properties();

    /**
     * Searches for property with specified name.
     *
     * @param name  property name to search for.
     * @return      value of the specified property.
     */
    public static String getProperty(String name) {
        try (InputStream in = AppProperties.class.getClassLoader().getResourceAsStream(PROP_NAME)) {
            PROP.load(in);
            return PROP.getProperty(name);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
