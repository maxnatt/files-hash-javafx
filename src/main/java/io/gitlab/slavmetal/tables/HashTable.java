/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.tables;

import io.gitlab.slavmetal.DatabaseData;
import io.gitlab.slavmetal.DbConnection;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Contains methods for accessing the HASH table.
 */
public class HashTable {
    /**
     * Gets all existing entries in the HASH table for default connection.
     *
     * @return list of all entries.
     */
    public static List<DatabaseData> getAll() {
        return getAll(DbConnection.getConnection());
    }

    /**
     * Gets all existing entries in the HASH table for specified connection.
     *
     * @param conn  connection to perform operation on.
     * @return list of all entries.
     */
    private static List<DatabaseData> getAll(Connection conn) {
        String selectSql = "SELECT * FROM HASH ";
        try (PreparedStatement preparedStatement = conn.prepareStatement(selectSql);
             ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            List<DatabaseData> hashes = new ArrayList<>();
            while (resultSet.next()) {
                hashes.add(new DatabaseData(resultSet.getInt("HASH_ID"),
                        resultSet.getString("FILE_PATH"),
                        resultSet.getString("FILE_HASH"),
                        AlgTable.getName(resultSet.getInt("ALG_ID")),
                        resultSet.getBoolean("IS_DIRECTORY"),
                        resultSet.getBoolean("META_USED"),
                        resultSet.getTimestamp("TIME")));
            }

            return hashes;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    /**
     * Inserts new row to the HASH table for default connection.
     *
     * @param file          file/directory that was used for generating a hash.
     * @param hash          hash of file/directory.
     * @param algorithm     algorithm that was used for generating a hash.
     * @param isDirectory   whether item is a directory.
     * @param metaUsed      whether meta-data was used for generating hash of a directory.
     * @throws IOException  when path to file/directory can't be found.
     */
    public static void insert(File file, String hash, String algorithm, boolean isDirectory, boolean metaUsed)
            throws IOException {
        insert(file, hash, algorithm, isDirectory, metaUsed, DbConnection.getConnection());
    }

    /**
     * Inserts new row to the HASH table for specified connection.
     *
     * @param file          file/directory that was used for generating a hash.
     * @param hash          hash of file/directory.
     * @param algorithm     algorithm that was used for generating a hash.
     * @param isDirectory   whether item is a directory.
     * @param metaUsed      whether meta-data was used for generating hash of a directory.
     * @param conn  connection to perform operation on.
     * @throws IOException  when path to file/directory can't be found.
     */
    private static void insert(File file, String hash, String algorithm, boolean isDirectory, boolean metaUsed, Connection conn)
            throws IOException {

        int algId = AlgTable.getId(algorithm);

        if (algId == -1) {
            throw new IllegalArgumentException("ID of this algorithm couldn't be found");
        } else {
            String insertSql = String.format("INSERT INTO HASH (FILE_PATH, FILE_HASH, ALG_ID, IS_DIRECTORY, META_USED, TIME) " +
                            "VALUES ('%s', '%s', '%s', %s, %s, '%s')",
                    Objects.requireNonNull(file).getCanonicalPath(), Objects.requireNonNull(hash), algId,
                    isDirectory, metaUsed, new Timestamp(System.currentTimeMillis()));
            try (PreparedStatement preparedStatement = conn.prepareStatement(insertSql)) {
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Deletes specified row from the HASH table for default connection.
     *
     * @param item data to delete.
     */
    public static void delete(DatabaseData item) {
        delete(item, DbConnection.getConnection());
    }

    /**
     * Deletes specified row from the HASH table for specified connection.
     *
     * @param conn  connection to perform operation on.
     * @param item data to delete.
     */
    private static void delete(DatabaseData item, Connection conn) {
        String deleteSql = String.format("DELETE FROM HASH " +
                "WHERE HASH_ID = %d", item.getFileId());
        try (PreparedStatement preparedStatement = conn.prepareStatement(deleteSql)) {
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets matching row based on item's path, algorithm, meta and directory properties for default connection.
     *
     * @param filePath  path to the file/directory.
     * @param algName   name of the algorithm used for hashing.
     * @param isDir     whether item is a directory.
     * @param metaUsed  whether meta-data was used for generating hash of a directory.
     * @return          matching row.
     */
    public static DatabaseData getMatching(String filePath, String algName, boolean isDir,
                                                       boolean metaUsed) {
        return getMatching(filePath, algName, isDir, metaUsed, DbConnection.getConnection());
    }

    /**
     * Gets matching row based on item's path, algorithm, meta and directory properties for specified connection.
     *
     * @param filePath  path to the file/directory.
     * @param algName   name of the algorithm used for hashing.
     * @param isDir     whether item is a directory.
     * @param metaUsed  whether meta-data was used for generating hash of a directory.
     * @param conn  connection to perform operation on.
     * @return          matching row.
     */
    private static DatabaseData getMatching(String filePath, String algName, boolean isDir,
                                            boolean metaUsed, Connection conn) {
        String selectSql = String.format("SELECT * " +
                        "FROM HASH " +
                        "WHERE FILE_PATH = '%s' AND ALG_ID = '%d' AND IS_DIRECTORY = '%b' AND META_USED = '%b'",
                filePath, AlgTable.getId(algName), isDir, metaUsed);

        try (PreparedStatement ps = conn.prepareStatement(selectSql);
             ResultSet rs = ps.executeQuery()
        ) {
            if (rs.next()) {
                return new DatabaseData(rs.getInt("HASH_ID"),
                        rs.getString("FILE_PATH"),
                        rs.getString("FILE_HASH"),
                        AlgTable.getName(rs.getInt("ALG_ID")),
                        rs.getBoolean("IS_DIRECTORY"),
                        rs.getBoolean("META_USED"),
                        rs.getTimestamp("TIME"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Updates specified HASH table row with new hash value for default connection.
     *
     * @param id        row ID to update.
     * @param newHash   new hash of specified row ID.
     */
    public static void update(int id, String newHash) {
        update(id, newHash, DbConnection.getConnection());
    }

    /**
     * Updates specified HASH table row with new hash value for specified connection.
     *
     * @param id        row ID to update.
     * @param conn  connection to perform operation on.
     * @param newHash   new hash of specified row ID.
     */
    private static void update(int id, String newHash, Connection conn) {
        String updateSql = String.format("UPDATE HASH " +
                "SET FILE_HASH = '%s', TIME = '%s' " +
                "WHERE HASH_ID = %d", newHash, new Timestamp(System.currentTimeMillis()), id);
        try (PreparedStatement preparedStatement = conn.prepareStatement(updateSql)){
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
