/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.tables;

import io.gitlab.slavmetal.DbConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Contains methods for accessing the ALG table.
 */
class AlgTable {
    /**
     * Gets ID of an algorithm by its name for default connection.
     *
     * @param name  name of an algorithm to search for.
     * @return      ID of requested algorithm.
     */
    static int getId(String name) {
        return getId(name, DbConnection.getConnection());
    }

    /**
     * Gets ID of an algorithm by its name for specified connection.
     *
     * @param name  name of an algorithm to search for.
     * @param conn  connection to perform operation on.
     * @return      ID of requested algorithm.
     */
    static int getId(String name, Connection conn) {
        String selectSql = String.format("SELECT ALG_ID " +
                "FROM ALGORITHM " +
                "WHERE ALG_NAME = '%s'", name);

        try (PreparedStatement preparedStatement = conn.prepareStatement(selectSql);
             ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            resultSet.next();
            return resultSet.getInt("ALG_ID");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return -1;
    }

    /**
     * Gets name of an algorithm by its ID for default connection.
     *
     * @param id    ID of an algorithm to search for.
     * @return      name of requested algorithm.
     */
    static String getName(int id) {
        return getName(id, DbConnection.getConnection());
    }

    /**
     * Gets name of an algorithm by its ID for specified connection.
     *
     * @param id    ID of an algorithm to search for.
     * @param conn  connection to perform operation on.
     * @return      name of requested algorithm.
     */
    static String getName(int id, Connection conn) {
        String selectSql = String.format("SELECT ALG_NAME " +
                "FROM ALGORITHM " +
                "WHERE ALG_ID = '%d'", id);

        try (PreparedStatement preparedStatement = conn.prepareStatement(selectSql);
             ResultSet resultSet = preparedStatement.executeQuery()
        ) {
            resultSet.next();
            return resultSet.getString("ALG_NAME");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
