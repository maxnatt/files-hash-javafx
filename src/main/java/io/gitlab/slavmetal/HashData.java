/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal;

import javafx.beans.property.SimpleStringProperty;

/**
 * Defines data in the names/hashes JavaFX TableView.
 */
public final class HashData {
    /**
     * Name of a file/directory.
     */
    private final SimpleStringProperty fileName = new SimpleStringProperty();

    /**
     * Hash of a file/directory.
     */
    private final SimpleStringProperty fileHash = new SimpleStringProperty();

    /**
     * Tooltip of a file/directory.
     */
    private final String tooltip;

    /**
     * @param fileName  name of a file/directory.
     * @param fileHash  hash of a file/directory.
     * @param tooltip   tooltip of file/directory.
     */
    public HashData(String fileName, String fileHash, String tooltip) {
        this.fileName.set(fileName);
        this.fileHash.set(fileHash);
        this.tooltip = tooltip;
    }

    /**
     * @return name of a file/directory.
     */
    public String getFileName() {
        return fileName.get();
    }

    /**
     * @return hash of a file/directory.
     */
    public String getFileHash() {
        return fileHash.get();
    }

    /**
     * @return tooltip (e.g. list of files used for hashing a directory).
     */
    public String getTooltip() {
        return tooltip;
    }
}
