/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Contains utilities to work with files/directories.
 */
public final class FileUtils {
    private final static Logger logger = LoggerFactory.getLogger(FileUtils.class);

    /**
     * Returns all files (including those in subfolders).
     *
     * @param dir   directory to search for all files in.
     * @return      list of all files inside the specified directory.
     */
    public static List<File> getDirectoryFiles(File dir) {
        if (dir.isDirectory() && dir.canRead()) {
            try {
                // Use Java NIO to get all the files (including subfolders)
                return Files.walk(dir.toPath())
                        .filter(f -> f.toFile().isFile())
                        .map(Path::toFile)
                        .collect(Collectors.toList());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                logger.warn(String.format("%s is not a directory or current user has no rights to read it, skipping", dir.getCanonicalPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        return Collections.emptyList();
    }
}
