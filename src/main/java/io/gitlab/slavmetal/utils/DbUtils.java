/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.utils;

import io.gitlab.slavmetal.AppProperties;
import io.gitlab.slavmetal.DbConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Contains database utilities.
 */
public class DbUtils {
    private static final Logger logger = LoggerFactory.getLogger(DbUtils.class);

    /**
     * Checks if DB file specified in properties exists in user's home folder and if not, creates one.
     *
     * @throws IOException if path doesn't belong to a file or program can't read/write to it.
     */
    public static void createDbIfNotExists() throws IOException {
        File dbFile = new File(System.getProperty("user.home") + System.getProperty("file.separator") + AppProperties.getProperty("dbBasePath") + ".mv.db");

        if (dbFile.exists()) {
            if (!dbFile.isFile()) {
                // Path exists, but is not a file
                logger.error("Check if the right file is on the database path");
                throw new IOException("Database path doesn't belong to a file");
            } else if (!dbFile.canWrite() || !dbFile.canRead()) {
                // Path exists and it's a file, but program doesn't have enough permissions
                logger.error("Check database file permissions");
                throw new IOException("Not enough permissions to read or write to the database file");
            }
        } else {
            // DB file doesn't exist, create one
            logger.info("DB file doesn't exist, creating new one.");
            createDb();
        }
    }

    /**
     * Creates DB with two tables and data used in this program.
     */
    private static void createDb() {
        String createAlg = "create table ALGORITHM" +
                "(" +
                "  ALG_ID   SMALLINT auto_increment," +
                "  ALG_NAME VARCHAR  not null," +
                "  constraint ALGORITHM_PK" +
                "    primary key (ALG_ID)" +
                ");";
        String insertAlg = "INSERT INTO PUBLIC.ALGORITHM (ALG_NAME) VALUES ('SDBM');" +
                "INSERT INTO PUBLIC.ALGORITHM (ALG_NAME) VALUES ('DJB');" +
                "INSERT INTO PUBLIC.ALGORITHM (ALG_NAME) VALUES ('FNV');";
        String createHash = "create table HASH" +
                "(" +
                "  HASH_ID      INTEGER   auto_increment," +
                "  FILE_PATH    VARCHAR   not null," +
                "  FILE_HASH    VARCHAR   not null," +
                "  ALG_ID       SMALLINT  not null," +
                "  IS_DIRECTORY BOOLEAN   not null," +
                "  META_USED    BOOLEAN   not null," +
                "  TIME         TIMESTAMP not null," +
                "  constraint HASH_PK" +
                "    primary key (HASH_ID)," +
                "  constraint HASH_ALGORITHM_ALG_ID_FK" +
                "    foreign key (ALG_ID) references ALGORITHM" +
                ");";
        try (PreparedStatement psAlg = DbConnection.getConnection().prepareStatement(createAlg);
             PreparedStatement psHash = DbConnection.getConnection().prepareStatement(createHash))
        {
            psAlg.executeUpdate();
            psHash.executeUpdate();
            try (PreparedStatement psInsertAlg = DbConnection.getConnection().prepareStatement(insertAlg)) {
                psInsertAlg.executeUpdate();
            }
            logger.info("Finished creating tables and inserting starting data.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
