/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.controllers;

import io.gitlab.slavmetal.DatabaseData;
import io.gitlab.slavmetal.tables.HashTable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableView;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

/**
 * Controller of the DbView.
 */
public class DbController {

    /**
     * JavaFX table with data from a DB.
     */
    @FXML private TableView<DatabaseData> hashTable;

    /**
     * List containing data for the {@link DbController#hashTable}.
     */
    private ObservableList<DatabaseData> data = FXCollections.observableArrayList();

    /**
     * System's clipboard.
     */
    private final Clipboard clipboard = Clipboard.getSystemClipboard();

    /**
     * System's clipboard content.
     */
    private final ClipboardContent clipboardContent = new ClipboardContent();

    /**
     * Initialize elements on creation.
     */
    @FXML
    public void initialize() {
        // Track changes of ObservableList
        data = FXCollections.observableArrayList(HashTable.getAll());
        hashTable.setItems(data);

        // Create context menu items for TableView
        MenuItem copyIdItem = new MenuItem("Copy ID");
        MenuItem copyPathItem = new MenuItem("Copy path");
        MenuItem copyHashItem = new MenuItem("Copy hash");
        MenuItem copyAlgItem = new MenuItem("Copy algorithm");
        MenuItem copyTimeItem = new MenuItem("Copy time");
        SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();
        MenuItem removeRowItem = new MenuItem("Remove row");

        // And set their actions:
        // Copy ID
        copyIdItem.setOnAction((ActionEvent event) -> {
            DatabaseData item = hashTable.getSelectionModel().getSelectedItem();
            clipboardContent.putString(String.valueOf(item.getFileId()));
            clipboard.setContent(clipboardContent);
        });

        // Copy path
        copyPathItem.setOnAction((ActionEvent event) -> {
            DatabaseData item = hashTable.getSelectionModel().getSelectedItem();
            clipboardContent.putString(String.valueOf(item.getFileName()));
            clipboard.setContent(clipboardContent);
        });

        // Copy hash
        copyHashItem.setOnAction((ActionEvent event) -> {
            DatabaseData item = hashTable.getSelectionModel().getSelectedItem();
            clipboardContent.putString(String.valueOf(item.getFileHash()));
            clipboard.setContent(clipboardContent);
        });

        // Copy algorithm
        copyAlgItem.setOnAction((ActionEvent event) -> {
            DatabaseData item = hashTable.getSelectionModel().getSelectedItem();
            clipboardContent.putString(String.valueOf(item.getFileAlg()));
            clipboard.setContent(clipboardContent);
        });

        // Copy time
        copyTimeItem.setOnAction((ActionEvent event) -> {
            DatabaseData item = hashTable.getSelectionModel().getSelectedItem();
            clipboardContent.putString(String.valueOf(item.getTimestamp()));
            clipboard.setContent(clipboardContent);
        });

        // Remove item
        removeRowItem.setOnAction((ActionEvent event) -> {
            DatabaseData item = hashTable.getSelectionModel().getSelectedItem();
            HashTable.delete(item);
            data.remove(item);
        });

        // Create context menu and assign it to the table
        ContextMenu menu = new ContextMenu();
        menu.getItems().addAll(copyIdItem, copyPathItem, copyHashItem, copyAlgItem, copyTimeItem, separatorMenuItem, removeRowItem);
        hashTable.setContextMenu(menu);
    }
}
