/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.controllers;

import io.gitlab.slavmetal.DbConnection;
import io.gitlab.slavmetal.alerts.AboutAlert;
import io.gitlab.slavmetal.DatabaseData;
import io.gitlab.slavmetal.HashData;
import io.gitlab.slavmetal.alerts.NotMatchingHashAlert;
import io.gitlab.slavmetal.exceptions.WrongSelectedAlgorithmException;
import io.gitlab.slavmetal.hashes.DjbHash;
import io.gitlab.slavmetal.hashes.FnvHash;
import io.gitlab.slavmetal.hashes.HashGenerator;
import io.gitlab.slavmetal.hashes.SdbmHash;
import io.gitlab.slavmetal.tables.HashTable;
import io.gitlab.slavmetal.utils.FileUtils;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * Controller of the HashView.
 */
public class HashController {
    private static final Logger logger = LoggerFactory.getLogger(HashController.class);

    /**
     * List containing data for the {@link HashController#fileTable}.
     */
    private final ObservableList<HashData> data = FXCollections.observableArrayList();

    /**
     * Label with text for the status bar.
     */
    @FXML private Label statusLabel;

    /**
     * JavaFX table with file names and hashes.
     */
    @FXML private TableView<HashData> fileTable;

    /**
     * Group of RadioMenuItem buttons for choosing an algorithm.
     */
    @FXML private ToggleGroup algGroup;

    /**
     * Shows whether we should use files' metadata when hashing a directory.
     */
    @FXML private CheckMenuItem metadataDirCheck;

    /**
     * App's menu on the top.
     */
    @FXML private MenuBar menuBar;

    /**
     * Initialize elements on creation.
     */
    @FXML
    public void initialize() {
        // Track changes of ObservableList
        fileTable.setItems(data);

        // Show tooltips for rows
        fileTable.setRowFactory(r -> new TableRow<HashData>() {
            private final Tooltip tooltip = new Tooltip();
            public void updateItem(HashData data, boolean empty) {
                // Allow multiple lines and specify max size
                tooltip.setWrapText(true);
                tooltip.setMaxSize(600, 600);
                tooltip.setTextOverrun(OverrunStyle.ELLIPSIS);

                // Handle tooltip setting
                super.updateItem(data, empty);
                if (data == null) {
                    setTooltip(null);
                } else {
                    tooltip.setText(data.getTooltip());
                    setTooltip(tooltip);
                }
            }
        });
    }

    /**
     * Exits the app.
     */
    public void exitApp() {
        DbConnection.destroyConnection();
        Platform.exit();
    }

    /**
     * Shows modal dialog with information about this program.
     */
    public void showAbout() {
        AboutAlert.showAbout();
    }

    /**
     * Shows dialog to choose file(s) and processes hash(es) based on chosen parameters.
     */
    public void processFiles() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select File");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));

        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(menuBar.getScene().getWindow());
        if (selectedFiles != null) {
            logger.debug("Selected files: " + selectedFiles.toString());
            fillTableAndAddToDb(selectedFiles);
        }

        setStatusText(String.format("Finished processing file(s) with %s algorithm", getHashRadioButtonText()));
    }

    /**
     * Shows dialog to choose a directory and processes hash based on chosen parameters.
     */
    public void processDirectory() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select Directory");
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));

        File selectedDirectory = directoryChooser.showDialog(menuBar.getScene().getWindow());
        if (selectedDirectory != null) {
            try {
                logger.debug("Selected directory: " + selectedDirectory.getCanonicalPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            fillTableAndAddToDb(selectedDirectory);
        }

        setStatusText(String.format("Finished processing directory with %s algorithm%s",
                getHashRadioButtonText(),
                isMetaSelected() ? " using meta-data" : ""
                ));
    }

    /**
     * Calculates hash of passed files, fills table in the HashView, adds results to the database.
     *
     * @param list list of files to calculate hash.
     */
    private void fillTableAndAddToDb(List<File> list) {
        if (list != null) {
            // Empty the table before inserting new set of data
            data.clear();

            for (File file : list) {
                try {
                    // TODO multithreading
                    String hash = getHashClass().getFileHash(file);
                    if (hash != null) {
                        // Check if hash of this file already exists
                        checkDbAndAddOrUpdate(file, hash, false, false);
                        // Add file's name and calculated hash to the table
                        data.add(new HashData(file.getName(), hash, file.getCanonicalPath()));
                    } else {
                        logger.error("Something went wrong and hash wasn't created, see logs");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Calculates hash of passed directory, fills table in the HashView, adds result to the database.
     *
     * @param directory directory to calculate hash.
     */
    private void fillTableAndAddToDb(File directory) {
        if (directory != null) {
            // Empty the table before inserting new set of data
            data.clear();

            try {
                // Get hash based on whether metadata usage is enabled or not
                String hash;
                if (isMetaSelected())
                    hash = getHashClass().getDirectoryMetaHash(directory);
                else
                    hash = getHashClass().getDirectoryHash(directory);

                if (hash != null) {
                    // Check if hash of this directory already exists
                    checkDbAndAddOrUpdate(directory, hash, true, isMetaSelected());
                    // Add directory's name, calculated hash and tooltip to the table
                    data.add(new HashData(directory.getName(),
                            hash,
                            Objects.requireNonNull(FileUtils.getDirectoryFiles(directory)).toString()
                    ));
                } else {
                    logger.error("Something went wrong and hash wasn't created, see logs");
                }
            } catch (WrongSelectedAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Checks if database has matching row. If yes and hash is different, show alert and update hash.
     * Otherwise just add new row to the table.
     *
     * @param item          file or directory to search for in the database.
     * @param hash          current hash of the file or directory.
     * @param isDirectory   whether passed item is directory.
     * @param metaUsed      whether meta-data was used for generating the hash (always <code>false</code> for files).
     */
    private void checkDbAndAddOrUpdate(File item, String hash, boolean isDirectory, boolean metaUsed) {
        DatabaseData existing = null;
        try {
            existing = HashTable.getMatching(item.getCanonicalPath(), getHashRadioButtonText(), isDirectory, metaUsed);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (existing == null) {
            // It does not exist yet, just add it to the DB
            addDbEntry(item, hash, isDirectory, metaUsed);
        } else {
            // Entry exists, compare hash and (if needed) update
            if (!existing.getFileHash().equals(hash)) {
                NotMatchingHashAlert.showHashWarning(item, existing.getTimestamp());
                HashTable.update(existing.getFileId(), hash);
            }
        }
    }

    /**
     * Adds data to the hashes database, automatically passes chosen algorithm name.
     *
     * @param file          the file/directory used for hashing.
     * @param hash          the hash of this file/directory.
     * @param isDirectory   whether the entity used for hashing is a directory.
     * @param metaUsed      whether metadata was used for hashing (always <code>false</code> for files).
     */
    private void addDbEntry(File file, String hash, boolean isDirectory, boolean metaUsed) {
        try {
            HashTable.insert(file, hash, getHashRadioButtonText(), isDirectory, metaUsed);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns <code>true</code> if CheckMenuItem for meta-data usage in directories is checked.
     *
     * @return <code>true</code> if CheckMenuItem for meta-data usage in directories is checked;
     *         <code>false</code> otherwise
     */
    private boolean isMetaSelected() {
        return metadataDirCheck.isSelected();
    }

    /**
     * @return text value of chosen hash algorithm radio button.
     */
    private String getHashRadioButtonText() {
        return ((RadioMenuItem) algGroup.getSelectedToggle()).getText();
    }

    /**
     * @return                                  class representing chosen algorithm.
     * @throws WrongSelectedAlgorithmException  if no suitable algorithm name found.
     */
    private HashGenerator getHashClass() throws WrongSelectedAlgorithmException {
        switch (getHashRadioButtonText()) {
            case "SDBM":
                return new SdbmHash();
            case "DJB":
                return new DjbHash();
            case "FNV":
                return new FnvHash();
            default:
                throw new WrongSelectedAlgorithmException("Wrong algorithm radio button value");
        }
    }

    /**
     * Sets text of the label in the status bar.
     *
     * @param status text to set.
     */
    private void setStatusText(String status) {
        statusLabel.setText(status);
    }

    /**
     * Opens modal window to view and modify hashes database.
     */
    public void showDbView() {
        FXMLLoader fXMLLoader = new FXMLLoader();
        fXMLLoader.setLocation(getClass().getClassLoader().getResource("fxml/DbView.fxml"));
        Stage stage = new Stage();
        Scene scene = null;

        try {
            scene = new Scene(fXMLLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        stage.setScene(scene);
        stage.getIcons().add(new Image(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("img/app-128.png"))));
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("View and edit hash database");
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        stage.show();
    }
}
