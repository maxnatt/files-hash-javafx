/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.alerts;

import javafx.scene.control.Alert;

/**
 * Contains 'About' modal dialog (alert).
 */
public class AboutAlert {
    /**
     * Shows 'About' alert.
     */
    public static void showAbout(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About Hash Files");
        alert.setHeaderText("Hash Files 0.1a");
        alert.setContentText("SlavMetal \n2019");
        alert.showAndWait();
    }
}