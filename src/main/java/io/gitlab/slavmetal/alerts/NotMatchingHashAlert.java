/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.alerts;

import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Objects;

public class NotMatchingHashAlert {
    /**
     * Shows warning about not matching hash with path and time.
     *
     * @param file      file which has been changed.
     * @param timestamp last timestamp of hash update.
     */
    public static void showHashWarning(File file, Timestamp timestamp) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.setTitle("Existing hash doesn't match");
        alert.setHeaderText("Existing hash doesn't match");

        // Get the Stage.
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        // Add a custom icon.
        stage.getIcons().add(new Image(Objects.requireNonNull(NotMatchingHashAlert.class.getClassLoader().getResourceAsStream("img/warn-128.png"))));

        // Set content
        try {
            alert.setContentText(String.format("It looks like hash of the %s has changed since %s. Database will be updated with new hash and time.",
                    file.getCanonicalPath(),
                    timestamp));
        } catch (IOException e) {
            e.printStackTrace();
        }

        alert.showAndWait();
    }
}
