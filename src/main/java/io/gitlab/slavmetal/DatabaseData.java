/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal;

import javafx.beans.property.*;

import java.sql.Timestamp;

/**
 * Defines data in the database TableView.
 */
public class DatabaseData {
    /**
     * Unique ID of an entry.
     */
    private final SimpleIntegerProperty fileId = new SimpleIntegerProperty();

    /**
     * Name of a file in the entry.
     */
    private final SimpleStringProperty fileName = new SimpleStringProperty();

    /**
     * Hash of a file in the entry.
     */
    private final SimpleStringProperty fileHash = new SimpleStringProperty();

    /**
     * Algorithm used for hashing the file (it's name).
     */
    private final SimpleStringProperty fileAlg = new SimpleStringProperty();

    /**
     * Whether entry represents a directory.
     */
    private final SimpleBooleanProperty isDirectory = new SimpleBooleanProperty();

    /**
     * Whether meta-data was used for generating a hash (always <code>false</code> for files).
     */
    private final SimpleBooleanProperty metaUsed = new SimpleBooleanProperty();

    /**
     * Timestamp of insert/update of the entry.
     */
    private final SimpleObjectProperty<Timestamp> timestamp = new SimpleObjectProperty<>();

    /**
     * @param fileId        id of a file/directory
     * @param fileName      name of a file/directory
     * @param fileHash      hash of a file/directory
     * @param fileAlg       algorithm used to hash a file/directory
     * @param isDirectory   is item a directory
     * @param metaUsed      is hash generated based on meta-data
     * @param timestamp     timestamp of adding this entry
     */
    public DatabaseData(int fileId, String fileName, String fileHash, String fileAlg, boolean isDirectory,
                        boolean metaUsed, Timestamp timestamp) {
        this.fileId.set(fileId);
        this.fileName.set(fileName);
        this.fileHash.set(fileHash);
        this.fileAlg.set(fileAlg);
        this.isDirectory.set(isDirectory);
        this.metaUsed.set(metaUsed);
        this.timestamp.set(timestamp);
    }

    /**
     * @return id of a file/directory.
     */
    public int getFileId() {
        return fileId.get();
    }

    /**
     * @return name of a file/directory.
     */
    public String getFileName() {
        return fileName.get();
    }

    /**
     * @return hash of a file/directory.
     */
    public String getFileHash() {
        return fileHash.get();
    }

    /**
     * @return algorithm's name used for hashing a file/directory.
     */
    public String getFileAlg() {
        return fileAlg.get();
    }

    /**
     * @return bool if object is file or directory.
     */
    public boolean getIsDirectory() {
        return isDirectory.get();
    }

    /**
     * @return bool if metadata was used to hash a directory.
     */
    public boolean isMetaUsed() {
        return metaUsed.get();
    }

    /**
     * @return last timestamp of hashing this file/directory.
     */
    public Timestamp getTimestamp() {
        return timestamp.get();
    }
}
