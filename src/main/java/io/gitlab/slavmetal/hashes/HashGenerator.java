/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.hashes;

import io.gitlab.slavmetal.utils.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Objects;

/**
 * Represents an interface for generating hash of certain objects.
 */
public interface HashGenerator {
    /**
     * Hashes with certain algorithm starting with provided hash value.
     *
     * @param hash  hash value to start with.
     * @param bytes bytes to hash.
     * @return      hash of provided bytes.
     */
    long getHash(long hash, byte[] bytes);

    /**
     * Hashes with certain algorithm starting with initial hash value.
     *
     * @param bytes bytes to hash.
     * @return      hash of provided bytes.
     */
    long getHash(byte[] bytes);

    /**
     * Hashes InputStream with certain algorithm starting with initial hash value.
     *
     * @param inputStream   InputStream to hash.
     * @return              hash of provided InputStream.
     */
    long getHash(InputStream inputStream);

    /**
     * Provides method for hashing a single file.
     *
     * @param file                      file to hash.
     * @return                          hex representation of hash.
     * @throws FileNotFoundException    if file can't be found.
     */
    default String getFileHash(File file) throws FileNotFoundException {
        if (file.isFile() && file.canRead()) {
            return Long.toHexString(getHash(new FileInputStream(file)));
        } else {
            try {
                LogHolder.logger.warn(String.format("%s is not a file or current user has no rights to read it, skipping", file.getCanonicalPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * Provides method for hashing a single directory.
     *
     * @param directory file to hash.
     * @return          hex representation of hash.
     */
    default String getDirectoryHash(File directory) {
        List<File> files = Objects.requireNonNull(FileUtils.getDirectoryFiles(directory));
        StringBuilder hashes = new StringBuilder();

        for (File file : files) {
            try {
                hashes.append(getFileHash(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        return Long.toHexString(getHash(hashes.toString().getBytes()));
    }

    /**
     * Provides method for hashing a single directory based on meta-data of its files.
     *
     * @param directory file to hash.
     * @return          hex representation of hash.
     */
    default String getDirectoryMetaHash(File directory) {
        List<File> files = Objects.requireNonNull(FileUtils.getDirectoryFiles(directory));
        StringBuilder info = new StringBuilder();

        try {
            for (File file : files) {
                BasicFileAttributes bfa = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                info.append(bfa.creationTime())
                        .append(bfa.lastAccessTime())
                        .append(bfa.lastModifiedTime())
                        .append(bfa.size())
                        .append(bfa.isRegularFile())
                        .append(bfa.isOther())
                        .append(bfa.isSymbolicLink());
            }

            return Long.toHexString(getHash(info.toString().getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}

final class LogHolder {
    /**
     * Logger for default implementations in the {@link HashGenerator} class.
     */
    static final Logger logger = LoggerFactory.getLogger(HashGenerator.class);
}