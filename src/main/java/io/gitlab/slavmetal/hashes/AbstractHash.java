/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.hashes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * Represents abstract class based on the HashGenerator for hashing algorithms.
 */
abstract class AbstractHash implements HashGenerator {

    private static final Logger logger = LoggerFactory.getLogger(AbstractHash.class);

    /**
     * Initial hash value.
     */
    private final long INITIAL_VALUE;

    /**
     * @param initialValue initial hash value.
     */
    AbstractHash(long initialValue) {
        this.INITIAL_VALUE = initialValue;
    }

    /**
     * Hashes with certain algorithm starting with initial hash value.
     *
     * @param bytes bytes to hash.
     * @return      hash of provided bytes.
     */
    @Override
    public long getHash(byte[] bytes) {
        return getHash(INITIAL_VALUE, bytes);
    }

    /**
     * Hashes InputStream with certain algorithm starting with initial hash value.
     *
     * @param inputStream   InputStream to hash.
     * @return              hash of provided InputStream.
     */
    @Override
    public long getHash(InputStream inputStream) {
        long hash = INITIAL_VALUE;
        try (BufferedInputStream bis = new BufferedInputStream(inputStream)){
            byte[] buffer = new byte[4096];
            int bytesRead;

            long startTime = System.nanoTime();
            while ((bytesRead = bis.read(buffer)) != -1) {
                hash = getHash(hash, Arrays.copyOfRange(buffer, 0, bytesRead));
            }
            logger.debug(String.format("Finished calculating hash of InputStream in %d ns", System.nanoTime() - startTime));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return hash;
    }
}
