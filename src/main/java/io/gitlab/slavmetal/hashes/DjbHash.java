/*
  This file is part of Files Hash JavaFX.

  Files Hash JavaFX is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Files Hash JavaFX is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Files Hash JavaFX. If not, see <http://www.gnu.org/licenses/>.
 */

package io.gitlab.slavmetal.hashes;

/**
 * Represents simple and not secure DJB algorithm.
 */
public final class DjbHash extends AbstractHash {
    /**
     * Default DJB hash value.
     */
    private static final long INITIAL_VALUE = 5381;

    /**
     * Default constructor.
     */
    public DjbHash() {
        super(INITIAL_VALUE);
    }

    /**
     * Hashes with DJB algorithm starting with provided hash value.
     *
     * @param hash  hash value to start with.
     * @param bytes bytes to hash.
     * @return      hash of provided bytes.
     */
    @Override
    public long getHash(long hash, byte[] bytes) {
        long newHash = hash;

        for (byte b : bytes) {
            newHash = ((hash << 5) + hash) + b;
        }

        return newHash;
    }
}
